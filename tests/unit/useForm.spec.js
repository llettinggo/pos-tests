import { shallowMount } from '@vue/test-utils';
import UseForm from '@/components/UseForm.vue';

describe('Component UseForm', () => {
  it('is a valid component ', () => {
    const wrapper = shallowMount(UseForm);
    expect(wrapper.exists()).toBe(true);
  });

  describe('Fields', () => {
    it('should has a input email', () => {
      const wrapper = shallowMount(UseForm);
      expect(wrapper.find('.useForm_input_email').exists()).toBe(true);
    });

    it('should has a input idade', () => {
      const wrapper = shallowMount(UseForm);
      expect(wrapper.find('.useForm_input_idade').exists()).toBe(true);
    });

    it('should has a select so', () => {
      const wrapper = shallowMount(UseForm);
      expect(wrapper.find('.useForm_select_so').exists()).toBe(true);
    });
  });

  describe('Validations', () => {
    beforeAll(() => {
      localStorage.setItem('Email', 'email_100@gmail.com');
    });

    describe('Input email', () => {
      describe('When email is the same as login', () => {
        it('should be true', () => {
          const wrapper = shallowMount(UseForm, {
            data: () => ({ email: 'email_100@gmail.com' }),
          });
          expect(wrapper.vm.isValidEmail).toBe(true);
        });
      });
      describe('When email is NOT the same as login', () => {
        it('should be false', () => {
          const wrapper = shallowMount(UseForm, {
            data: () => ({ email: 'email_10@gmail.com' }),
          });
          expect(wrapper.vm.isValidEmail).toBe(false);
        });
      });
    });

    describe('Input idade', () => {
      describe('When idade is between 18 and 99', () => {
        it('should be true and NOT show a invalidation message', () => {
          const wrapper = shallowMount(UseForm, {
            data: () => ({ idade: '25' }),
          });
          expect(wrapper.vm.isValidIdade).toBe(true);
        });
      });

      describe('When idade is NOT between 18 and 99', () => {
        it('should be false and show a invalidation message', () => {
          const wrapper = shallowMount(UseForm, {
            data: () => ({ idade: '14' }),
          });
          expect(wrapper.vm.isValidIdade).toBe(false);
        });
      });
    });

    describe('Select So', () => {
      describe('When so is NOT empty', () => {
        it('should be true', () => {
          const wrapper = shallowMount(UseForm, {
            data: () => ({ so: 'Windows' }),
          });
          expect(wrapper.vm.isValidSo).toBe(true);
        });
      });

      describe('When so is empty', () => {
        it('should be false', () => {
          const wrapper = shallowMount(UseForm, {
            data: () => ({ so: '' }),
          });
          expect(wrapper.vm.isValidSo).toBe(false);
        });
      });
    });

    afterAll(() => {
      localStorage.removeItem('Email');
    });
  });

  describe('Congrats message', () => {
    beforeAll(() => {
      localStorage.setItem('Email', 'email_10@gmail.com');
    });

    describe('When Email, Idade and So are correct filled', () => {
      it("should be true and show 'Parabéns' message", () => {
        const wrapper = shallowMount(UseForm, {
          data: () => ({ email: 'email_10@gmail.com', idade: '26', so: 'Windows' }),
        });
        expect(wrapper.vm.isFormCompleted).toBe(true);
      });
    });

    describe('When Email, Idade and So are NOT correct filled', () => {
      it("should NOT be true and show 'Parabéns' message", () => {
        const wrapper = shallowMount(UseForm, {
          data: () => ({ email: 'email_012@gmail.com', idade: '12', so: '' }),
        });
        expect(wrapper.vm.isFormCompleted).toBe(false);
      });
    });

    afterAll(() => {
      localStorage.removeItem('Email');
    });
  });
});

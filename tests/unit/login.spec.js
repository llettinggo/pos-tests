import { shallowMount } from '@vue/test-utils';
import Login from '@/components/Login.vue';

describe('Component Login', () => {
  it('is a valid component ', () => {
    const wrapper = shallowMount(Login);
    expect(wrapper.exists()).toBe(true);
  });

  describe('Fields', () => {
    it('should has a input email', () => {
      const wrapper = shallowMount(Login);
      expect(wrapper.find('.login_input_email').exists()).toBe(true);
    });

    it('should has a input password', () => {
      const wrapper = shallowMount(Login);
      expect(wrapper.find('.login_input_password').exists()).toBe(true);
    });
  });

  describe('Validations', () => {
    describe('Email', () => {
      describe('When email has a valid format', () => {
        it('should be true and NOT show a invalidation message', () => {
          const wrapper = shallowMount(Login, {
            data: () => ({ email: 'fulano@gmail.com' }),
          });
          expect(wrapper.vm.isValidEmail).toBe(true);
        });
      });
      describe('When email has NOT a valid format', () => {
        it('should be false and show a invalidation message', () => {
          const wrapper = shallowMount(Login, {
            data: () => ({ email: 'fulanogmail.com' }),
          });
          expect(wrapper.vm.isValidEmail).toBe(false);
        });
      });
    });

    describe('Password', () => {
      describe('When password length is not lesser than 6', () => {
        it('should be true and NOT show a invalidation message', () => {
          const wrapper = shallowMount(Login, {
            data: () => ({ password: '5456454' }),
          });
          expect(wrapper.vm.isValidPassword).toBe(true);
        });
      });

      describe('When password length is lesser than 6', () => {
        it('should be false and show a invalidation message', () => {
          const wrapper = shallowMount(Login, {
            data: () => ({ password: '545' }),
          });
          expect(wrapper.vm.isValidPassword).toBe(false);
        });
      });
    });
  });

  describe('CorrectLogin', () => {
    describe('When has email and password', () => {
      describe('And email and password are valids', () => {
        it('should be true and NOT show incorrect message', () => {
          const wrapper = shallowMount(Login, {
            data: () => ({ email: 'email_100@gmail.com', password: 'passord100' }),
          });
          expect(wrapper.vm.isCorrectLogin).toBe(true);
        });
      });

      describe('And email and password are NOT valids', () => {
        it('should be false and show incorrect message', () => {
          const wrapper = shallowMount(Login, {
            data: () => ({ email: 'email_012@gmail.com', password: 'passord012' }),
          });
          expect(wrapper.vm.isCorrectLogin).toBe(false);
        });
      });
    });
  });

  describe('Redirect Login', () => {
    describe('When email and password are correct', () => {
      it('should change route when click button with right login', async () => {
        const $router = {
          push: jest.fn(),
        };
        const wrapperWithParams = shallowMount(Login, {
          data: () => ({ email: 'email_10@gmail.com', password: 'password10' }),
          mocks: {
            $router,
          },
        });
        wrapperWithParams.find('.login_button_goTo').trigger('click');

        await wrapperWithParams.vm.$forceUpdate();
      });
    });
    describe('When email and password are NOT correct', () => {
      it('should show incorrect message', async () => {
        const $router = {
          push: jest.fn(),
        };
        const wrapper = shallowMount(Login, {
          data: () => ({ email: 'email_10@gmail.com', password: 'password102' }),
          mocks: {
            $router,
          },
        });
        wrapper.find('.login_button_goTo').trigger('click');

        await wrapper.vm.$forceUpdate();
      });
    });
  });
});

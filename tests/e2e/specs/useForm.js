
describe('UseForm', () => {
  describe('When visit form url', () => {
    it('contains h1 UseForm', () => {
      cy.visit('/useForm');
      cy.contains('h1', 'UseForm');
    });
    describe("When fill corretly email and idade and select some 'So", () => {
      beforeEach(() => {
        localStorage.setItem('Email', 'email_10@gmail.com');
      });
      it("should show 'Parabéns' message", () => {
        cy.visit('/useForm');
        cy.get('.useForm_input_email').type('email_10@gmail.com');
        cy.get('.useForm_input_idade').type('46');
        cy.get('.useForm_select_so').select('Linux');
      });
      afterEach(() => {
        localStorage.removeItem('Email');
      });
    });
    describe('When NOT fill corretly email and idade and select some so option', () => {
      beforeEach(() => {
        localStorage.setItem('Email', 'email_10@gmail.com');
      });
      it("should NOT show 'Parabéns' message", () => {
        cy.visit('/useForm');
        cy.get('.useForm_input_email').type('email_010@gmail.com');
        cy.get('.useForm_input_idade').type('4');
        cy.get('.useForm_select_so').select('');
      });
      afterEach(() => {
        localStorage.removeItem('Email');
      });
    });
  });
});

describe('Login', () => {
  describe('When visit form url', () => {
    it('contains h1 Login', () => {
      cy.visit('/login');
      cy.contains('h1', 'Login');
    });

    describe('When fill corretly email and password', () => {
      it('should got to UseForm When click button', () => {
        cy.visit('/login');
        cy.get('.login_input_email').type('email_100@gmail.com');
        cy.get('.login_input_password').type('passord100');
        cy.get('.login_button_goTo').click();
        cy.url().should('eq', 'http://localhost:8080/useForm'); // => true
      });
    });

    describe('When NOT fill corretly email and password', () => {
      it('should show incorrect message', () => {
        cy.visit('/login');
        cy.get('.login_input_email').type('email_010@gmail.com');
        cy.get('.login_input_password').type('passord100');
        cy.get('.login_button_goTo').click();
      });
    });
  });
});
